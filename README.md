# Structure of ResNet

project of machine learning, group 2, Qian Li

**This piece of code is able to run without requiring too much computating power. Please notice that there are only limited samples, the model would learn nothing, its result is different from that in project report. The results in the report are given by models trained with thoudsands of samples.**
## 1.1 ml-project_final.ipynb
## 1.2 meta info files
1. train.csv,  image_id, class_name, class_id, rad_id, x_min, y_min, x_max, y_max
1. train_meta.csv, image_id, dim0(width), dim1(height)
1. train_merged.csv, image_id, class_name, class_id, rad_id, x_min, y_min, x_max, y_max, dim_x, dim_y
1. train_set1.csv,image_id, class_id(0:normal, 1:abnormal) 
1. train_set2.csv,image_id, class_id_0,class_id_3,class_id_11,class_id_13
1. train_selected.csv, subset of train_set1.csv, contains 1024 rows
1. binary_train.csv, training dataset for binary classifier
1. binary_test.csv, test dataset for binary classifier
1. multi_label_train.csv, training dataset for multi-label classifier
1. multi_label_test.csv, test dataset for multi-label classifier

## 1.3 train, containing images for training and test 

## 1.4 concerned classes:
1. 14: no finding
1. 0: Aortic enlargement
1. 3: Cardiomegaly
1. 11: Pleural thickening
1. 13: Pulmonary fibrosis